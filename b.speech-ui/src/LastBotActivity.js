import React from 'react';
import buildSSML from './buildSSML.js';
import { connectToWebChat, Components } from 'botframework-webchat';

const { SpeakActivity } = Components;

// let rate=Math.ceil((1-speechScore)*1000)+"%"

export default connectToWebChat(({ activities }) => {
  return({
    activityBot: activities.slice().reverse().find(({ from: { role }, type }) => role === 'bot' && type === 'message'),
    activityUser: activities.slice().reverse().find(({ from: { role }, type }) => role === 'user' && type === 'message')
  })})(
      ({ activityBot,activityUser }) =>{
          if (typeof activityBot!=="undefined"){
              let speechScore = activityUser.channelData.speech.alternatives[0].confidence
              // let intentScore = activityUser.intentScore
              let lang="en-US"
              let pitch=0.9
              let rate='100%'
              let text=activityBot.text
              let voice="es-MX-JorgeNeural"
              let volume=1
              // console.log("activityUser",activityUser);
              // console.log("activityBot",activityBot);
              activityBot.speak = buildSSML(lang,pitch,rate,text,voice,volume)
              // console.log(buildSSML(lang,pitch,rate,text,voice,volume));

              //activityBot.speak = '<speak xmlns="http://www.w3.org/2001/10/synthesis" xmlns:mstts="http://www.w3.org/2001/mstts" xmlns:emo="http://www.w3.org/2009/10/emotionml" version="1.0" xml:lang="en-US"><voice name="es-MX-JorgeNeural"><prosody rate="100%" pitch="-5%">Excellent! Which day of the week can you visit?</prosody></voice></speak>';
          }
          return !!activityBot && (
          <React.Fragment>
            <p>{activityBot.text}</p>
            {activityBot.channelData && activityBot.channelData.speak && <SpeakActivity activity={activityBot}/>}
          </React.Fragment>
        )}
    );


// Original Code
 // import React from 'react';
//
// import { connectToWebChat, Components } from 'botframework-webchat';
//
// const { SpeakActivity } = Components;
//
// export default connectToWebChat(({ activities }) => ({
//   activity: activities
//     .slice()
//     .reverse()
//     .find(({ from: { role }, type }) => role === 'bot' && type === 'message')
// }))(
//   ({ activity }) =>
//     !!activity && (
//       <React.Fragment>
//         <p>{activity.text}</p>
//         {<SpeakActivity activity={activity} />}
//       </React.Fragment>
//     )
// );
