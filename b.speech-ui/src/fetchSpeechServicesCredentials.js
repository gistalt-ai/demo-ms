const RENEW_EVERY = 300000;
let fetchPromise;
let lastFetch = 0;

// async function region() {
//   const { region } = await fetchCredentials();
//
//   return region;
// }
//
// async function token() {
//   const { token } = await fetchCredentials();
//
//   return token;
// }
//
// // This fetch function will be called every time Web Speech recognizer or synthesizer start
// // You are advised to cache the token to prevent unnecessary network call and delay
// async function fetchCredentials() {
//   const now = Date.now();
//
//   if (!fetchPromise || now - lastFetch > RENEW_EVERY) {
//     fetchPromise = fetch('https://webchat-mockbot.azurewebsites.net/speechservices/token', { method: 'POST' },)
//       .then(res => res.json())
//       .catch(() => {
//         lastFetch = 0;
//       });
//
//     lastFetch = now;
//   }
//
//   return fetchPromise;
// }
// Test lines 34-62
// // const res = await fetch('https://directline.botframework.com/v3/directline/tokens/generate', { method: 'POST', headers: { Authorization: 'Bearer my-directline-secret-key' } });
// // const { token } = await res.json();
//
// async function token() {
//   const { token } = await fetchCredentials();
//
//   return token;
// }
//
// const region = 'eastus';
//
// let newToken;
//
// async function fetchCredentials() {
//   const tokenRes = await fetch(
//   'https://eastus.api.cognitive.microsoft.com/sts/v1.0/issuetoken',
//   { method: 'POST', headers: { 'Ocp-Apim-Subscription-Key': 'f22b0b47495749aa9e8e1e179f402d78' } }
//   );
//
//   if (tokenRes.status === 200) {
//       newToken = await tokenRes.text()
//   }
//   return { "token": newToken };
// }

  // else {
  //     return (new Error('error!'))
  // }

async function region() {
  const region = "eastus"

  return region;
}

async function token() {
  const token  = await fetchCredentials();

  return token;
}

// Andrew bot: f22b0b47495749aa9e8e1e179f402d78
// Clau bot: e35745a247784726a9628f376b060b80
async function fetchCredentials() {
  const now = Date.now();

  if (!fetchPromise || now - lastFetch > RENEW_EVERY) {
    fetchPromise = fetch('https://eastus.api.cognitive.microsoft.com/sts/v1.0/issueToken',
    { method: 'POST', headers: {"Content-type": "application/x-www-form-urlencoded", "Content-length": "0", "Ocp-Apim-Subscription-Key": "e35745a247784726a9628f376b060b80"}})
      .then(res => res.text())
      .catch(() => {
        lastFetch = 0;
      });

    lastFetch = now;
  }

  return fetchPromise;
}

export default fetchCredentials;
export { region, token };
